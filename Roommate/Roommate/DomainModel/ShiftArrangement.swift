//
//  ShiftArrangement.swift
//  Final_Project
//
//  Created by 許景程 on 2019/5/9.
//  Copyright © 2019 EnChang. All rights reserved.
//

import Foundation

class ShiftArrangement{
    //Mark: 屬性
    private var user: User
    private var arrangementItem: ArrangementItem
    
    //Mark: 初始化
    init(user: User, arrangementItem: ArrangementItem) {
        self.user = user
        self.arrangementItem = arrangementItem
    }
    
}
