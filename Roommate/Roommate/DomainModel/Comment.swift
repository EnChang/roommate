//
//  Comment.swift
//  Roommate
//
//  Created by 許景程 on 2019/5/22.
//  Copyright © 2019 EnChang. All rights reserved.
//

import Foundation

class Comment: NSObject {
    var votes: Int = 0
    var commentContent: String?
    var author: String?
    var timeStamp: String?
}
