//
//  Announcement.swift
//  Final_Project
//
//  Created by 許景程 on 2019/5/9.
//  Copyright © 2019 EnChang. All rights reserved.
//

import Foundation

class Announcement{
    //MARK: 屬性
    private var id: Int
    private var user: User
    private var date: Data
    private var comment: String
    
    //MARK: 初始化
    init(id: Int, user: User, date: Data, comment: String) {
        self.id = id
        self.user = user
        self.date = date
        self.comment = comment
    }
}
