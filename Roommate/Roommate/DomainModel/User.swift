//
//  User.swift
//  Final_Project
//
//  Created by 許景程 on 2019/5/8.
//  Copyright © 2019 EnChang. All rights reserved.
//

import Foundation

class User{
    //MARK: 屬性
    private var id: Int
    private var name: String
    private var account: String
    private var password: String
    private var score: Int
    
    //MARK: 初始化
    init(id: Int, name: String, account: String, password: String, score: Int) {
        self.id = id
        self.name = name
        self.account = account
        self.password = password
        self.score = score
    }
}
