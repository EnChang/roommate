//
//  Facility.swift
//  Final_Project
//
//  Created by 許景程 on 2019/5/8.
//  Copyright © 2019 EnChang. All rights reserved.
//

import Foundation

class Facility{
    //MARK: 屬性
    private var id: Int
    private var name: String
    
    //MARK: 初始化
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
    
    //Mark: 設定屬性
    var Id: Int{
        get{
            return self.id
        }
    }
    
    var Name: String{
        get{
            return self.name
        }
    }
}
