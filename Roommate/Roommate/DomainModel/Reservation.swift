//
//  Reservation.swift
//  Final_Project
//
//  Created by 許景程 on 2019/5/8.
//  Copyright © 2019 EnChang. All rights reserved.
//

import Foundation

class Reservarion{
    
    //MARK: 屬性
    private var id: Int
    private var startDate: String
    private var endDate: String
    private var facility: Facility
    
    //MARK: 初始化
    init(id: Int, startDate: String, endDate: String, facility: Facility ) {
        self.id = id
        self.startDate = startDate
        self.endDate = endDate
        self.facility = facility
    }
    
    //Mark: 設定屬性
    var Id: Int{
        get{
            return self.id
        }
    }
    
    var StartDate: String{
        get{
            return self.startDate
        }
    }
    
    var EndDate: String{
        get{
            return self.endDate
        }
    }
    
    var Facility: Facility{
        get{
            return self.facility
        }
    }
}
