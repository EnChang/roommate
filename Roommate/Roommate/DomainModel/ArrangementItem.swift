//
//  ArrangementItem.swift
//  Final_Project
//
//  Created by 許景程 on 2019/5/9.
//  Copyright © 2019 EnChang. All rights reserved.
//

import Foundation

class ArrangementItem{
    //Mark: 屬性
    private var id: Int
    private var name: String
    
    //Mark: 初始化
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
}
