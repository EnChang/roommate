//
//  SignUpViewController.swift
//  Rootmate
//
//  Created by 許景程 on 2019/5/16.
//  Copyright © 2019 EnChang. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class SignUpViewController: UIViewController {
 
    var dataCount = 0
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        userNameTextField.placeholder = "Enter Name"
        emailTextField.placeholder = "Enter email"
        passwordTextField.placeholder = "Enter password"
        // Do any additional setup after loading the view.
    }
    
    @IBAction func submitInformationAction(_ sender: UIButton) {
        if userNameTextField.text == "" || emailTextField.text == "" || passwordTextField.text == ""{
            let alertController = UIAlertController(title: "Error", message: "Please fill all the information", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)
        }else{
            Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) {
                (user, error) in
                
                if error == nil {
                    self.setUserInfo()
                    
                    let viewController = self.storyboard?.instantiateViewController(withIdentifier: "LogInViewController")
                    
                    self.present(viewController!, animated: true, completion: nil)
                }
                else{
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
                
            }
        }
    }
    
    func setUserInfo(){
        let reference: DatabaseReference! = Database.database().reference().child("User")
        var userInfo: [String : AnyObject] = [String : AnyObject]()
        
        userInfo["name"] = userNameTextField.text! as AnyObject
        userInfo["email"] = emailTextField.text! as AnyObject
        userInfo["password"] = passwordTextField.text! as AnyObject
        
        let emailString = emailTextField.text!
        let key = emailString.replacingOccurrences(of: ".", with: ",")
        
        let userInfoReference = reference.child(key)
        
        userInfoReference.updateChildValues(userInfo) {
            (err, ref) in
            if err != nil{
                print("err： \(err!)")
                
                return
            }
            
            print(ref.description())
        }
    }

}
