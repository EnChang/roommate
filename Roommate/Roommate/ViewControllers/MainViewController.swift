//
//  MainViewController.swift
//  Roommate
//
//  Created by 許景程 on 2019/5/21.
//  Copyright © 2019 EnChang. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let image = UIImage(named: "profile")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handleProfile))

        // Do any additional setup after loading the view.
    }
    
    @objc func handleProfile(){
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController")
        let navgationController = UINavigationController(rootViewController: viewController!)
        present(navgationController, animated: true, completion: nil)
    }

}
