//
//  CommentTableViewController.swift
//  Roommate
//
//  Created by 許景程 on 2019/5/21.
//  Copyright © 2019 EnChang. All rights reserved.
//

import UIKit
import Firebase

class CommentTableViewController: UITableViewController {
    let cellId = "cell"
    var comments = [Comment]()
    var newComment: Comment!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let editImage = UIImage(named: "edit")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: editImage, style: .plain, target: self, action: #selector(editMessage))
        tableView.estimatedRowHeight = 135.0
        tableView.rowHeight = UITableView.automaticDimension
        self.fetchComment()
    }
    
    func fetchComment(){
        Database.database().reference().child("Comment").observe(.childAdded, with: {
            (snapshot) in
            
            var newComments = [Comment]()
            
            if let dictionary = snapshot.value as? [String: AnyObject]{
                let comment = Comment()
                
                comment.author = dictionary["author"] as! String
                comment.commentContent = dictionary["commentContent"] as! String
                comment.votes = dictionary["votes"] as! Int
                comment.timeStamp = dictionary["timeStamp"] as! String
                self.comments.append(comment)
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            
        })
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        fetchComment()
//    }

    @objc func editMessage(){
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "NewCommentViewController")
        let navgationController = UINavigationController(rootViewController: viewController!)
        present(navgationController, animated: true, completion: nil)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return comments.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let comment = comments[indexPath.row]
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommentTableViewCell") as? NewCommentTableViewCell{
            cell.configuareCell(comment: comment)
            return cell
        }else{
            return NewCommentTableViewCell()
        }
    }
    


}
