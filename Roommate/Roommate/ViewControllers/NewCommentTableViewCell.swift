//
//  NewCommentTableViewCell.swift
//  Roommate
//
//  Created by 許景程 on 2019/5/22.
//  Copyright © 2019 EnChang. All rights reserved.
//

import UIKit

class NewCommentTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var commentCellTextView: UITextView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    var comment = Comment()
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configuareCell (comment: Comment){
        self.comment = comment
        
        self.commentCellTextView.text = comment.commentContent!
        self.userNameLabel.text = comment.author!
        self.timeLabel.text = comment.timeStamp!
        
    }
}
