//
//  NewCommentViewController.swift
//  Roommate
//
//  Created by 許景程 on 2019/5/21.
//  Copyright © 2019 EnChang. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class NewCommentViewController: UIViewController {

    var currentUserName = ""
    @IBOutlet weak var commentTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(handleCancel))
        navigationItem.title = "新增留言"
        // Do any additional setup after loading the view.
        let key = Auth.auth().currentUser?.email
        
        let userKey = key!.replacingOccurrences(of: ".", with: ",")
        let ref = Database.database().reference().child("User").child(userKey)
        
        ref.observeSingleEvent(of: .value, with: {(snapshot) in
            if !snapshot.exists() { return }
            
            if let dictionary = snapshot.value as? [String: AnyObject]{
                self.currentUserName = dictionary["name"]! as! String
            }
        })
    }


    @objc func handleCancel(){
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitCommentAction(_ sender: UIButton) {
        let newCommentText = commentTextView.text!
        
        if newCommentText != ""{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy/MM/dd HH:mm"
            var newComment: [String : AnyObject] = [String : AnyObject]()
            
            newComment["commentContent"] = newCommentText as AnyObject
            newComment["votes"] = 0 as AnyObject
            newComment["author"] = self.currentUserName as AnyObject
            newComment["timeStamp"] = dateFormatter.string(from: NSDate() as Date) as AnyObject
            let reference: DatabaseReference! = Database.database().reference().child("Comment")
            let commentReference = reference.childByAutoId()
            commentReference.updateChildValues(newComment)
            
            dismiss(animated: true, completion: nil)
        }
    }
}
