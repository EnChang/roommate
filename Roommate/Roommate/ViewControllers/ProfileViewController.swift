//
//  ProfileViewController.swift
//  Roommate
//
//  Created by 許景程 on 2019/5/21.
//  Copyright © 2019 EnChang. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(handleCancel))
        // Do any additional setup after loading the view.
    }
    
    @objc func handleCancel(){
        dismiss(animated: true, completion: nil)
    }
}
