//
//  ViewController.swift
//  Rootmate
//
//  Created by 許景程 on 2019/5/16.
//  Copyright © 2019 EnChang. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {

    @IBOutlet weak var signUpButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.signUpButton.layer.borderWidth = 1
        self.signUpButton.layer.borderColor = UIColor.white.cgColor
    }


}

